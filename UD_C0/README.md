

# Unidad C0: Recapitulación


                                    Autor: ACHILLE JEANLIN MAGANOU

Introducción (sobre el documento o el tema tratado) -- mejor escribirla al final.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Las bases de datos son una estructura organizada de información o datos que se pueden relacionar entre si, consultarse, almacenarse de forma electronica en un sistema.

Tratan de resolver problemas como el almacenamiento de datos ,la consulta de datos,modificar datos,borrar datos,relacionar datos, la peristencia y la concurrencia de datos. 

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

un sistema de gestión de bases de datos (DBMS) es una aplicación informatica alojada en un servidor que permite gestionar bases de datos de forma eficiente.

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB
* IMB Db2
* SQLite
* MariaDB
* SQL Server
* PostgreSQL
* mySQL

Oracle DB:

    Uso actual: Si.
    Software libre: No, Oracle DB es un software propietario.
    Modelo cliente-servidor: Sí, Oracle DB sigue el modelo cliente-servidor.

IBM Db2:

    Uso actual:  se usa casi a día de hoy
    Software libre: No, IBM Db2 es un software propietario.
    Modelo cliente-servidor: Sí, IBM Db2 sigue el modelo cliente-servidor.

SQLite:

    Uso actual: SQLite es ampliamente utilizado en aplicaciones móviles, navegadores web, sistemas embebidos y pequeñas aplicaciones de escritorio.
    Software libre: Sí, SQLite es de código abierto y está en dominio público.
    Modelo cliente-servidor: No, SQLite es una biblioteca de bases de datos sin servidor.

MariaDB:

    Uso actual: MariaDB es una alternativa a MySQL y se utiliza en una amplia gama de aplicaciones web y empresariales.
    Software libre: Sí, MariaDB es de código abierto y es una bifurcación de MySQL.
    Modelo cliente-servidor: Sí, MariaDB sigue el modelo cliente-servidor.

SQL Server:

    Uso actual: SI
    Software libre: No, SQL Server es un software propietario de Microsoft.
    Modelo cliente-servidor: Sí, SQL Server sigue el modelo cliente-servidor.

PostgreSQL:

    Uso actual: Si
    Software libre: Sí, PostgreSQL es de código abierto y tiene una gran comunidad de desarrolladores.
    Modelo cliente-servidor: Sí, PostgreSQL sigue el modelo cliente-servidor.

MySQL:

    Uso actual: MySQL es uno de los sistemas de gestión de bases de datos más populares y se utiliza en una amplia gama de aplicaciones web y empresariales.
    Software libre: Sí, MySQL es de código abierto y es propiedad de Oracle Corporation.
    Modelo cliente-servidor: Sí, MySQL sigue el modelo cliente-servidor.

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor?
    Porque es importante que los datos esten alojados o guardados en remoto para evitar posibles perdidas de datos.

 ¿Qué ventajas tiene desacoplar al DBMS del cliente? 
 
* Al desacoplar el DBMS del cliente, puedes implementar medidas de seguridad más sólidas. 

* Puedes escalar la base de datos y los servidores de aplicaciones de forma independiente para manejar cargas de trabajo crecientes sin afectar el rendimiento de los clientes. 

* Esto te permite utilizar la tecnología de base de datos más adecuada para tu aplicación o negocio en cualquier momento, sin tener que reescribir o modificar el código del cliente.

* Puedes realizar actualizaciones, parches y ajustes de configuración en el DBMS sin afectar a los clientes. Esto simplifica las operaciones de mantenimiento y reduce el riesgo de interrupciones del servicio.

 ¿En qué se basa el modelo cliente-servidor?

 En el modelo cliente-servidor, un servidor sirve a varios clientes y, por ende, procesa múltiples peticiones de diferentes clientes. Para ello, presta su servicio de forma permanente y pasiva. Por su parte, el cliente solicita activamente los servicios del servidor e inicia las tareas del servidor.

 * __Cliente__:

    un cliente se refiere a un dispositivo o aplicación que solicita y consume servicios, recursos o datos proporcionados por otro dispositivo o programa, conocido como servidor. 
* __Servidor__:

    Un servidor es un sistema de computación o software que proporciona servicios o datos a otros dispositivos o programas, conocidos como clientes, en una red. 

* __Red__:

    Una red es un conjunto de dispositivos informáticos interconectados que pueden comunicarse entre sí y compartir recursos y datos. 


* __Puerto de escucha__:

    Un puerto de escucha es un número de identificación asociado a un programa o servicio en un servidor que espera y "escucha" las conexiones entrantes de los clientes.En mysql el puerto es 3306.


* __Petición__:

    Una petición, en el contexto de las comunicaciones de red, es un mensaje enviado por un cliente a un servidor solicitando un servicio. Las solicitudes pueden ser de diferentes tipos según el protocolo utilizado (por ejemplo, HTTP, FTP, SMTP) y pueden incluir datos adicionales necesarios para completar la solicitud.

* __Respuesta__:

    Una respuesta es un mensaje enviado por un servidor a un cliente en respuesta a una solicitud recibida. 






## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

El SQL (Structured Query Language) mas que  un lenguaje de programación declarativo  , es un estandar desde 1986 de cuarta generacion que utilizan casi todas las bases de datos relacionales para consultar, manipular y definir los datos, además de para proporcionar control de acceso.
### Instrucciones de SQL

#### DDL

Los DLL(Data Definition Language) que permiten crear y definir nuevas bases de datos, campos e índices.


 |Comando  	|Descripción|
 |----------|---------- |
 |CREATE 	|   Utilizado para crear nuevas tablas, campos e índices
 |DROP 	  |Empleado para eliminar tablas e índices
 |ALTER 	|  Utilizado para modificar las tablas agregando campos o cambiando la definición de los campos.

#### DML

Los DML(Data Manipulation Language) que permiten generar consultas para ordenar, filtrar y extraer datos de la base de datos.


 |Comando  	|Descripción|
 |----------|---------- |
 |SELECT|se utiliza para consultar datos de una o varias tablas de una base de datos|
 |INSERT| para insertar nuevas filas de datos en una tabla|
 |UPDATE| para modificar datos existentes de una tabla|
 |DELETE| para eliminar datos de una tabla|

#### DCL
Los DCL(Data Control Language) que se encargan de definir las permisos sobre los datos

 
 |Comando  	|Descripción|
 |----------|---------- |
 |GRANT|Permite asignar permisos sobre el objeto de la base de datos|
 |REVOKE|Para quitar permisos|

#### TCL

 TCL (Transaction Control Language -Lenguaje Control de Transacciones) para las transacciones de datos


 |Comando  	|Descripción|
 |----------|---------- |
 |COMMIT|Finaliza la transacción y realiza los cambios hechos durante la transacción|
 |ROLLBACK| Rechaza la transacción y no aplica cambios, volviendo al estado antes de iniciarse la transacción|
 |SAVEPOINT|Crea un punto en la transacción que se pueda volver mediante ROLLBACK.|
 |SET TRANSACTION|Inicializa una transacción en la base de datos, indicando si quiere que sea de solo lectura o lectura/escritura.|



## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

Una base de datos relacional (RDB) es una forma de estructurar información en tablas, filas y columnas. Un RDB tiene la capacidad de establecer vínculos (o relaciones) entre datos mediante la unión de tablas.

Los elementos que la conforman son:
* __Relación (tabla)__: es una estructura donde se almacenan los datos en filas y columnas.
* __Atributo/Campo (columna)__: son las columnas de una tabla donde se puedan acceder a los datos.
* __Registro/Tupla (fila)__: es conjunto de campos de una tabla .

ventajas de uso :
* Permite administrar grandes cantidades de datos de forma eficiente y estructurada.
* Se puede escalar de forma horizontal lo que significa que se le pueden alojar en mas servidores mientras mas datos se añadan.
* Permite la persistencia de datos












En este documento vamos a tratar en general de las bases de datos. Primero hemos definido el concepto de bases de datos y de sistema gestor de base datos  y que problemas tratan de resolver.Hemos citado los principales sistemas gestores base de datos y luego hemos explicado el concepto del modelo cliente-servidor.Luego he definido el concepto de SQl y explicado sus principales instrucciones.Para acabar hemos explicado las bases de datos relacionales y que elementos la conforman.




