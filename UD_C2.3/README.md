Reto 2.3: Consultas con subconsultas II
[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas
Para este reto, volveremos a usar la base de datos Chinook (más información en
el Reto 2.1).

![Chinook](imagen.png)

Figure 1: Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-
database).
Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:
Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos
donde se espera un solo valor, como parte de una condición WHERE, SELECT,
HAVING, etc. Ejemplo:
Obtener una lista de empleados que ganan más que el salario medio de la empresa.
[1]
SELECT
lastname,
firstname,
salary
FROM employee
1
WHERE salary > (SELECT avg(salary)
FROM employee)

Consulta 1
Obtener las canciones con una duración superior a la media.

```sql
-- Obtener las canciones con una duración superior a la media
use Chinook;
SELECT Name Cancion
FROM Track

WHERE Milliseconds > (SELECT avg(Milliseconds)
					FROM Track)


```


Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es “emma_jones@hotmail.com”.
Subconsultas de varias filas
```sql

use Chinook;
SELECT *
FROM Invoice
WHERE CustomerId = (
    SELECT CustomerId
    FROM Customer
    WHERE Email = 'emma_jones@hotmail.com'
)
ORDER BY InvoiceDate DESC
LIMIT 5;

```
Diferenciamos dos tipos:
1. Subconsultas que devuelven una columna con múltiples filas (es decir, una
lista de valores). Suelen incluirse en la cláusula WHERE para filtrar los
resultados de la consulta principal. En este caso, suelen utilizarse con
operadores como IN, NOT IN, ANY, ALL, EXISTS o NOT EXISTS.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es
decir, tablas). Se comportan como una tabla temporal y se utilizan en
lugares donde se espera una tabla, como en una cláusula FROM. [2]
Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.
```sql

use Chinook;
SELECT *
FROM Playlist
WHERE PlaylistId IN (
    SELECT DISTINCT PlaylistId
    FROM PlaylistTrack
    WHERE TrackId IN (
        SELECT TrackId
        FROM Track
        WHERE GenreId = (
            SELECT GenreId
            FROM Genre
            WHERE Name = 'Reggae'
        )
    )
);


```
Consulta 4
Obtener la información de los clientes que han realizado compras superiores a
20€.
```sql

use Chinook;
SELECT *
FROM Customer
WHERE CustomerId IN (
	SELECT CustomerId
	FROM Invoice
	WHERE Total > 20
)


```
Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.
```sql

use Chinook;
SELECT AL.AlbumId, AL.Title AS Album, AR.Name AS Artista
FROM Album AL
JOIN Artist AR 
ON AL.ArtistId = AR.ArtistId
WHERE AL.AlbumId IN (
    SELECT AlbumId
    FROM Track
    GROUP BY AlbumId
    HAVING COUNT(*) > 15
);

```
Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.
```sql

use Chinook;
SELECT AlbumId,COUNT(*) AS NumeroCanciones
FROM Track
group by AlbumId
HAVING COUNT(*) > (
    SELECT AVG(NumeroCanciones)
    FROM (
        SELECT COUNT(*) AS NumeroCanciones
        FROM Track
        GROUP BY AlbumId
    ) AS SubQuery
);



```
Consulta 7
Obtener los álbumes con una duración total superior a la media.
```sql

use Chinook;
SELECT T.AlbumId ,Title,sum(milliseconds) as Duracion
FROM Track T
JOIN Album A ON  T.AlbumId = A.AlbumId
group by AlbumId
Having  Duracion > (
	SELECT avg(Duracion)
		FROM (
		SELECT AlbumId,sum(milliseconds) as Duracion
		FROM Track 
		group by AlbumId
		) AS Subquery 
);



```
Consulta 8
Canciones del género con más canciones.
```sql
SELECT TrackId, Name AS TrackName, GenreId
FROM Track
WHERE GenreId = (SELECT GenreId
					FROM Track
					GROUP BY GenreId
					ORDER BY COUNT(*) DESC
					LIMIT 1);

```
Consulta 9
Canciones de la playlist con más canciones.
```sql
SELECT T.TrackId, T.Name AS Cancion, P.Name AS Playlist
FROM Track T
JOIN PlaylistTrack ON T.TrackId = PlaylistTrack.TrackId
JOIN Playlist P  ON PlaylistTrack.PlaylistId = P.PlaylistId
WHERE PlaylistTrack.PlaylistId = (
    SELECT PlaylistId
    FROM PlaylistTrack
    GROUP BY PlaylistId
    ORDER BY COUNT(*) DESC
    LIMIT 1
);
```
Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa.
Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la
consulta externa, suponiendo una gran carga computacional. Ejemplo:
Supongamos que queremos encontrar a todos los empleados con un salario superior
al promedio de su departamento. [1]
SELECT
lastname,
firstname,
salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
FROM employee e2
WHERE e2.dept_id = e1.dept_id)
La principal diferencia entre una subconsulta correlacionada en SQL y una
subconsulta simple es que las subconsultas correlacionadas hacen referencia
a columnas de la tabla externa. En el ejemplo anterior, e1.dept_id es una
referencia a la tabla de la subconsulta externa. [1]

Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno
en compras.
```sql
SELECT FirstName AS PrimerApellido , LastName AS SegundoApellido,
    (
        SELECT COUNT(CustomerId)
        FROM Customer AS C
        WHERE C.SupportRepId = E.EmployeeId
    ) AS 'Total'
FROM Employee AS E;

```

Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.
```sql
SELECT FirstName AS PrimerApellido, LastName AS SegundoApellido,
    (
        SELECT COUNT(CustomerId)
        FROM Customer AS C
        WHERE C.SupportRepId = E.EmployeeId
    ) AS 'Total'
FROM Employee AS E;

```
Consulta 12
Ventas totales de cada empleado.
```sql
SELECT EmployeeId AS IdEmpleado,
    (
        SELECT 
            SUM(
                (SELECT SUM(Total)
                FROM Invoice AS I
                WHERE I.CustomerId = C.CustomerId)
            ) AS 'Total'
        FROM Customer AS C
        WHERE SupportRepId = E.EmployeeId
        GROUP BY SupportRepId
    )
FROM Employee E

```
Consulta 13
Álbumes junto al número de canciones en cada uno.
```sql
SELECT A.AlbumId AS IdAlbum, A.Title AS Titulo,
    (
        SELECT COUNT(AlbumId)
        FROM Track AS T
        WHERE T.AlbumId = A.AlbumId
    ) AS 'Número de canciones'
FROM Album AS A;

```
Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes
más antiguos tienen un ID más bajo.
```sql
SELECT ArtistId AS IdArtista , Name AS Nombre , Antigua
    FROM (
        SELECT Ar.ArtistId, Ar.Name,
            (
                SELECT Title
                FROM Album AS Al
                WHERE Al.ArtistId = Ar.ArtistId
                ORDER BY Al.AlbumId
                LIMIT 1
            ) AS Antigua
        FROM Artist AS Ar
    ) AS Subconsulta
WHERE Antigua IS NOT NULL;


```
3
Referencias
• [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-
para-principiantes/
• [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-
sql/