Reto 2.1 Consultas con funciones de agregación

![Chinook](imagen.png)

Consultas sobre una tabla

Query 1
Encuentra todos los clientes de Francia.

```sql
use Chinook;
SELECT *
FROM Customer
WHERE Country like 'France'
```

Query 2
Muestra las facturas del primer trimestre de este año.

```sql
use Chinook;
SELECT *
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(CURDATE()) AND QUARTER(InvoiceDate) = 1;


```

Query 3
Muestra todas las canciones compuestas por AC/DC.

```sql
use Chinook;
SELECT Name AS Cancion
FROM Track
WHERE Composer like 'AC/DC'
```

Query 4
Muestra las 10 canciones que más tamaño ocupan.

```sql
use Chinook;
SELECT Name AS Cancion
FROM Track
ORDER BY Bytes desc
LIMIT 10 
```

Query 5
Muestra el nombre de aquellos países en los que tenemos clientes.
```sql
use Chinook;
SELECT Country AS Pais
FROM Customer
group by Country
```

Query 6
Muestra todos los géneros musicales.
```sql
SELECT Name as Nombre
FROM Chinook.Genre

```
Consultas sobre múltiples tablas

Query 7
Muestra todos los artistas junto a sus álbumes.


```sql
use Chinook;
SELECT ar.Name AS nombre  ,AL.Title AS Albumes
FROM Artist ar
JOIN Album AL ON ar.ArtistId = AL.ArtistId


```
Query 8
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen.
```sql
use Chinook;
SELECT concat(e1.LastName,' ',e1.FirstName) AS empleado, concat(e2.LastName,'  ',e2.FirstName) AS supervisor
FROM Employee e1
INNER JOIN Employee e2 
ON e1.EmployeeId = e2.ReportsTo
Order by e1.BirthDate DESC
LIMIT 15

```

Query 9
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las colum-nas: fecha de la factura, nombre completo del cliente, dirección de facturación,código postal, país, importe (en este orden).
```sql
use Chinook;
SELECT i.InvoiceDate AS fecha , concat(c.FirstName,' ',c.LastName) AS Nombrecompleto , i.BillingAddress AS dirección, i.BillingPostalCode AS códigopostal, i.BillingCountry AS país, i.Total AS importe
FROM Invoice i
JOIN Customer c ON i.CustomerId = c.CustomerId
WHERE BillingCity like 'Berlin'
```

Query 10
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración.
```sql
SELECT P.PlaylistId, P.NAme AS 'Nombre', PT.PlaylistId,PT.TrackId, T.TrackId AS 'IdCanción', T.Name AS 'Canción', T.Milliseconds AS 'Milisegundos'
FROM Playlist  P
JOIN PlaylistTrack  PT ON P.PlaylistId = PT.PlaylistId
JOIN Track  T ON PT.TrackId = T.TrackId
WHERE P.Name LIKE 'C%'
ORDER BY P.PlaylistId DESC, T.Milliseconds DESC;

```

Query 11
Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.
```sql
SELECT C.CustomerId AS 'IdCliente',C.FirstName AS 'Nombre',C.LastName AS 'Apellido',I.InvoiceId AS 'IdFactura',I.CustomerId AS 'IdCliente',I.Total AS 'Precio'
FROM Customer  C
JOIN Invoice  I
ON C.CustomerId = I.CustomerId
WHERE I.Total > 10
ORDER BY C.LastName ASC;
```

Consultas con funciones de agregación

Query 12
Muestra el importe medio, mínimo y máximo de cada factura.


```sql
use Chinook;
SELECT avg(Total) Importe_medio,min(Total) minimo,max(Total) maximo
FROM Invoice

```

Query 13
Muestra el número total de artistas.
```sql
use Chinook;
SELECT count(Name) Total
FROM Artist

```

Query 14
Muestra el número de canciones del álbum “Out Of Time”.
```sql

```

Query 15
Muestra el número de países donde tenemos clientes.
```sql
use Chinook;
SELECT count(DISTINCT Country) Total
FROM Customer

```

Query 16
Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).
```sql
use Chinook;
SELECT COUNT(DISTINCT T.Name) AS NumeroDeCanciones,G.Name as Genero
FROM Genre G
JOIN Track T ON G.GenreId = T.GenreId
GROUP BY G.Name

```

Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
```sql

SELECT A.Title AS 'Título', COUNT(A.Title) AS 'Número de canciones'
FROM Album AS A
JOIN Track AS T ON A.AlbumId = T.AlbumId
GROUP BY A.Title;

```

Query 18
Encuentra los géneros musicales más populares (los más comprados).
```sql
SELECT G.Name AS 'Nombre',  COUNT(G.GenreId) AS 'TotalVentas'
FROM Genre AS G 
JOIN Track AS T ON G.GenreId = T.GenreId
JOIN InvoiceLine AS IL ON T.TrackId = IL.TrackId
GROUP BY G.GenreId
ORDER BY Ventas DESC
LIMIT 10;

```

Query 19
Lista los 6 álbumes que acumulan más compras.


```sql
SELECT A.Title AS 'Título', COUNT(A.Title) AS 'TotalVentas'
FROM Album AS A 
JOIN Track AS T ON A.AlbumId = T.AlbumId
JOIN InvoiceLine AS IL ON T.TrackId = IL.TrackId
GROUP BY A.Title
ORDER BY Ventas DESC
LIMIT 6;

```
Query 20
Muestra los países en los que tenemos al menos 5 clientes

```sql

SELECT Country AS 'Paises', COUNT(Country)
FROM Customer
GROUP BY Country
HAVING COUNT(Country) >= 5;

```