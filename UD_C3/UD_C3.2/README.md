Tarea UD_C3: Conexión, DDL (Definición de datos) y DCL (Control de acceso)

Reto 3.2 : DCL Control de acceso

En este tarea,vamos a explorar las opciones de control de acceso que ofrece MySQL.
Para eso documentaremos como registrar nuevos usuarios, modificarlos y eliminarlos,cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD,cómo mostrar los usuarios existentes y sus permisos,que permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.,que permisos hacen falta para gestionar los usuarios y sus permisos,la posibilidad de agrupar los usuarios (grupos/roles/...) ,que comandos usamos para gestionar los usuarios y sus permisos.

Para conectarse a una base de datos con la CLI (Interfaz Gráfica de Comandos) usamos el comando :
    
    mysql -u root -p -P 33006 -h 127.0.0.1

Para mostrar los usuarios existentes en el Sistema de Gestion de Bases de Datos usamos el comando :
    
    SELECT user FROM mysql.user;

Para ver las columnas de esta tabla usamos:

    SHOW COLUMNS FROM mysql.user;

Para mostrar todos los usuarios existentes y desde que host te puedes conectar usamos el comando:
    
    SELECT Host, User FROM mysql.user;

Para mostrar los privilegios de los usuarios que existen en mysql. usaremos el comando :
    
    SHOW PRIVILEGES;

  Hay dos tipos de privilegios que existen: 'dinámicos' y 'estáticos'. Vamos a trabajar con los estáticos ya que los dinámicos se utilizan en momento de ejecución, los estáticos nos permiten leer tablas, no podamos leer tablas, se puedan hacer selects.

Para ver los privilegios de cada usuarios accedemos recurrimos a las tablas GRANT(significa 'acceder' en inglés). Se puede usar el comando ´SHOW GRANTS´ de la siguiente manera:

    SHOW GRANTS FOR 'root'@'localhost';

Pecularidad: en mysql los usuarios vienen identificados con el nombre de usuarios y el host de donde venga como se puede ver en el comando de arriba. Son como usuarios ya que pueden restringir o garántizar el acceso a diferentes hosts.
Se puede apreciar que nos lo separa ya que los primeros son los privilegios estáticos y los siguiente los dinámicos.
HAY QUE TENER (Con Chinook ese):
crear usuario y asignar privilegios para Chinook ver la tablas concretas sin poder modificar pero la de playlist sí. Podemos añadir permisos al usuairo en el host concreto. Añadir permisos a nivel de grupos de usuarios (utilizando patrones ('%', 'admin_')). Lo más interesante son los roles, a los roles se les pueden añadir privilegios (rola admin, rol usuario, etc...) y a los usuarios que se registren se les asigna el rol de usuario.
Interesante:
Quitarle permisos a un usuario (REVOKE), Qué pasa si se renombra un usuario o como actualizar la contra de un usuario (ALTER USER). Con REVOKE ALL se eliminan todos los privilegios y con GRANT ALL se le asignan.
Hay que poner tipos de autenticación a parte de una contraseña(token, etc..).
Bloquear usuarios (ALTER USER lock para bloquearlo o unlock para desbloquear (no verificado)).

Para crear un nuevo usuario en mysql ejecutamos este comando :
    
    CREATE USER 'achilelgoleador'@'localhost' IDENTIFIED BY 'password';

Para crear un nuevo usuario en mysql y que solo lo podamos usar en nuestra red local:

    CREATE USER 'achilelgoleador'@'192.168.%' IDENTIFIED BY 'password';


En este momento, newuser no tiene permisos para hacer nada con las bases de datos. De hecho, incluso si newuser intenta iniciar sesión (con la contraseña, password), no podrá acceder al shell de MySQL.

Por lo tanto, lo primero que se debe hacer es proporcionar al usuario acceso a la información que necesitará.

    GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';

Los asteriscos en este comando se refieren a la base de datos y la tabla (respectivamente) a los que pueden acceder. Este comando específico permite al usuario leer, editar, ejecutar y realizar todas las tareas en todas las bases de datos y tablas.

Tenga en cuenta que en este ejemplo estamos otorgando a newuser acceso de root completo a todo en nuestra base de datos. Si bien esto es útil para explicar algunos conceptos de MySQL, puede ser poco práctico para la mayoría de casos de uso y podría poner la seguridad de su base de datos en alto riesgo.

Una vez que haya finalizado los permisos que desea configurar para sus nuevos usuarios, asegúrese siempre de volver a cargar todos los privilegios.

    FLUSH PRIVILEGES;

Sus cambios ahora estarán vigentes.


Cómo otorgar diferentes permisos de usuario

Aquí se incluye una breve lista de otros posibles permisos comunes que los usuarios pueden disfrutar.

    ALL PRIVILEGES: Como vimos antes, esto le otorgaría a un usuario de MySQL acceso completo a una base de datos designada (o si no se selecciona ninguna base de datos, acceso global a todo el sistema).
    CREATE: Permite crear nuevas tablas o bases de datos.
    DROP: Permite eliminar tablas o bases de datos.
    DELETE: Permite eliminar filas de las tablas.
    INSERT: Permite insertar filas en las tablas.
    SELECT: Les permite usar el comando SELECT para leer las bases de datos.
    UPDATE: Permite actualizar las filas de las tablas.
    GRANT OPTION: Permite otorgar o eliminar privilegios de otros usuarios.

Para proporcionar un permiso a un usuario específico, puede usar este marco:
GRANT type_of_permission ON database_name.table_name TO 'username'@'localhost';

Si desea darle a un usuario acceso a cualquier base de datos o a cualquier tabla, asegúrese de poner un asterisco (*) en el lugar del nombre de la base de datos o de la tabla.

Cada vez que actualice o cambie un permiso, asegúrese de usar el comando Flush Privileges.

Si necesita revocar un permiso, la estructura es casi la misma que para otorgar un permiso:
REVOKE type_of_permission ON database_name.table_name FROM 'username'@'localhost';

Al igual que puede eliminar bases de datos con DROP, también puede usar DROP para eliminar un usuario por completo:

    DROP USER 'username'@'localhost';

Para probar su nuevo usuario, cierre sesión escribiendo lo siguiente:

quit

Dar privilegios a un usuario a una base de datos y todas sus tablas:
       
        GRANT SELECT ON Chinook.* TO 'root'@'192.168.%'

Quitar permisos/privilegios a un usuario a una base de datos llamada Chinook y su tabla Employee(Empleados):
        
        REVOKE SELECT ON Chinook.Employee FROM 'root@192.168.*'

Rol
Podemos crear roles para facilitar la integración de permisos a los nuevos usuarios.
Para crear un rol :

CREATE ROLE 'user';
Se le pueden asignar premisos a los roles de la misma manera que a un usuario:
      GRANT SELECT ON Chinook.Artist TO 'user'

    Una vez tenemos el Rol creado ya podemos asignar los diferentes permisos sobre el mismo, tal cual vimos en los puntos anteriores:

        GRANT EXECUTE ON *.* TO  mantenimineto_total;   -- Permiso a nivel global
        GRANT SELECT ON employees.* TO mantenimiento_basico;   -- Permiso a nivel de base de datos
        GRANT SELECT ON employees.departments TO consulta_basica;  -- Permiso a nivel de tabla


Añadir un usario a un rol :

        GRANT 'user' TO 'root'@%;


    Fijarse que dicha asignación no lleva consigo que el usuario tenga los permisos asociados a los roles que ahora tiene.

    Es necesario activar dichos roles.
    Se puede comprobar como al acceder como el usuario 1, si ejecutamos la función CURRENT_ROLE() devuelve NONE:

        SELECT current_role();



    La activación de un rol la podemos implementar de dos formas:

        Dejarla activada por defecto y cada vez que el usuario se conecta, los permisos del rol están activos para ese usuario.
        Que sea el usuario (o el administrador) el que active/desactive el rol.

    Para activar por defecto un rol, necesitamos emplear la orden SET DEFAULT ROLE.

Mod BD ud8 roles 4.jpg

    El mismo usuario puede activar o desactivar los roles por defecto o bien un administrador con permisos CREATE USER, o UPDATE en la tabla del sistema mysql.default_roles.
    Por ejemplo, para activar todos los roles asignados a un usuario podemos poner:

        SET DEFAULT ROLE ALL TO 'user1'@'%','user2'@'%';

    Para activar un rol concreto:

        SET DEFAULT ROLE mantenimineto_total TO 'user2'@'%';

    Esto hará que cuando el usuario user2 se conecte tenga activados todos los roles asignados (mantenimiento_basico,consulta_basica) y que el usuario user2 tenga activado por defecto el rol mantenimineto_total.


    Si queremos activar/desactivar un rol mientras estamos conectados, es el propio usuario que deberá de ejecutar la orden SET ROLE.

Mod BD ud8 roles 5.jpg

    Por ejemplo:

        SET ROLE "usuario_raso"l;   -- Activar el role a usuario_raso ESTANDO CONECTADO COMO usuario_raso

        SET ROLE NONE;   -- Deactiviaría todos los roles a user2 ESTANDO CONECTADO COMO user2

Nota: Podéis comprobar en la sintaxis de SET ROL que existen diferentes formas de activar y desactivar múltiples rol


        

        Añadir un usario a un rol: 
        SET ROLE ;