Tarea UD_C3: Conexión, DDL (Definición de datos) y DCL (Control de acceso)

Reto 3.1 DDL - Definición de datos en MySQL

En este tarea,vamos a definir la estructura de datos para una aplicación de gestión de vuelos, que tendrá las tablas Vuelos, Pasajeros y Reservas.

Siguiendo el ejemplo propuesto, documentaremos el proceso de definición de la estructura de datos, así como la inserción de algunos registros en la base de datos creada. Reflexionaremos sobre las restricciones (constraints) definidas, sobre las claves foráneas, así como sobre la estructura de datos en sí.
Reflexionaremos sobre las ventajas e inconvenientes de las restricciones definidas a la hora de cancelar un vuelo, de asignar un mismo asiento a dos pasajeros diferentes, o de asignar un pasajero a un vuelo inexistente. Y responderemos alas siguientes preguntas: ¿Qué restricciones son útiles en cada caso? ¿Qué otras situaciones sería conveniente que manejara el SGBD para asegurar la integridad de los datos? 

Para definir la estructura de datos para una aplicación de gestión de vuelos utilizando las tablas `Vuelos`, `Pasajeros` y `Reservas`, vamos a analizar cada tabla y las relaciones entre ellas. Aquí está el proceso de definición de la estructura de datos y algunas reflexiones sobre las restricciones:

### 1. Tabla `Vuelos`:
- Esta tabla almacena la información sobre los vuelos disponibles.
- Clave primaria: `id_vuelo`.
- No hay claves foráneas en esta tabla.

Pa eso ejectutamos esta query :
```sql
CREATE TABLE `Vuelos` (
  `id_vuelo` int unsigned NOT NULL AUTO_INCREMENT,
  `origen` varchar(50) NOT NULL DEFAULT '',
  `destino` varchar(50) NOT NULL DEFAULT '',
  `fecha` date NOT NULL DEFAULT (0),
  `capacidad` int NOT NULL DEFAULT (0),
  PRIMARY KEY (`id_vuelo`)
) 

´´´

### 2. Tabla `Pasajeros`:
- Esta tabla almacena la información sobre los pasajeros.
- Clave primaria: `id_pasajero`.
- No hay claves foráneas en esta tabla.


Pa eso ejectutamos esta query :
```sql
CREATE TABLE `Pasajeros` (
  `id_pasajero` int unsigned NOT NULL AUTO_INCREMENT,
  `numero_pasaporte` varchar(50) NOT NULL DEFAULT '',
  `nombre_pasajero` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_pasajero`,`numero_pasaporte`) USING BTREE,
  UNIQUE KEY `numero_pasaporte_UNIQUE` (`numero_pasaporte`)
) 
´´´
### 3. Tabla `Vuelos_Pasajeros`:
- Esta tabla representa las reservas de vuelos, relacionando los pasajeros con los vuelos en una relación de muchos a muchos(N:M).
- Clave primaria: `id_reserva`.
- Claves foráneas:
  - `id_vuelo` referencia a `Vuelos.id_vuelo`.
  - `id_pasajero` referencia a `Pasajeros.id_pasajero`.
- También se incluye la columna `n_asiento` para registrar el asiento asignado a cada pasajero en un vuelo específico.

Pa eso ejectutamos esta query :
```sql
CREATE TABLE `Vuelos_Pasajeros` (
  `id_reserva` int NOT NULL,
  `id_vuelo` int unsigned NOT NULL,
  `id_pasajero` int unsigned NOT NULL,
  `n_asiento` int DEFAULT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `id_pasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),
  CONSTRAINT `id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)
)
´´´
### Reflexiones sobre las restricciones definidas:

1. **Necesidad de tres tablas**:
   - Utilizamos tres tablas para separar las entidades principales de nuestro sistema: vuelos, pasajeros y reservas. Esto permite una mejor organización y evita la redundancia de datos.

2. **Claves primarias y foráneas**:
   - Las claves primarias garantizan la unicidad de cada registro en una tabla.
   - Las claves foráneas establecen relaciones entre las tablas, asegurando la integridad referencial de los datos.

3. **Restricciones definidas**:
   - Las restricciones de clave foránea aseguran que cada reserva esté asociada a un vuelo y un pasajero existentes.
   - La restricción de clave primaria en la tabla `Reservas` garantiza la unicidad de cada reserva.
   - La restricción de unicidad en la tabla `Pasajeros` del campo `pasaporte` asegura que no haya dos pasajeros con el mismo número de pasaporte.

### Ventajas e inconvenientes de las restricciones:

1. **Cancelación de un vuelo**:
    Una restricción de clave foránea puede ser útil para garantizar que no se puedan realizar reservas en vuelos cancelados.

2. **Asignación del mismo asiento a dos pasajeros diferentes**:
   Una validación adicional podría ser necesaria para asegurar que un asiento no se asigne a más de un pasajero en el mismo vuelo.

3. **Asignación de un pasajero a un vuelo inexistente**:
  La clave foránea en la tabla `Reservas` garantiza que un pasajero solo pueda reservar vuelos existentes, evitando así reservas en vuelos que no están en la base de datos.

4. **Otras situaciones a considerar**:
   - Sería útil que el SGBD  elimine automáticamente las reservas asociadas a un vuelo cancelado o a un pasajero eliminado sería como un delete automatico.

En resumen, las restricciones definidas aseguran la integridad de los datos y ayudan a mantener la consistencia en la base de datos. Sin embargo, es importante considerar otros escenarios y posibles restricciones adicionales según las necesidades específicas del sistema.