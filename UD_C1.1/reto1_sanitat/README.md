# Reto 1: Consultas básicas

Achille Jeanlin Maganou.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/AchRealG/b-bdd/-/blob/main/UD_C1/reto1_sanitat/README.md

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;

SELECT HOSPITAL_COD,NOM,TELEFON
FROM HOSPITAL 
```


## Query 2

Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`y que tengan
una letra A en la segunda posición del nombre. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;

SELECT HOSPITAL_COD,NOM,TELEFON
FROM HOSPITAL 
WHERE NOM LIKE '_A%'
```

## Query 3

Para seleccionar el código hospital, el código sala, el número de empleado y el apellido existentes, seleccionaremos estos cuatro atributos, que se corresponden con las columnas `HOSPITAL_COD`, `SALA_COD`, y `EMPLEAT_NO`,`COGNOM` respectivamente, de la tabla `PLANTILLA`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;

SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM

FROM PLANTILLA
```

## Query 4

Para seleccionar el código hospital, el código sala, el número de empleado y el apellido existentes, seleccionaremos estos cuatro atributos, que se corresponden con las columnas `HOSPITAL_COD`, `SALA_COD`, y `EMPLEAT_NO`,`COGNOM` respectivamente, de la tabla `PLANTILLA`que no sean del turno de noche. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;

SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM

FROM PLANTILLA

WHERE TORN NOT LIKE 'N'
```

## Query 5

Para mostrar a los enfermos nacidos en 1960, seleccionaremos todos los atributos  de la tabla `MALALT`. Lo llevaremos a cabo con la siguiente sentencia SQL:
```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX ) = '1960'
```

## Query 6


Para mostrar a los enfermos nacidos a partir del año 1960, seleccionaremos todos los atributos  de la tabla `MALALT`. Lo llevaremos a cabo con la siguiente sentencia SQL:
```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX ) >= '1960'
```
